#include <iostream>
#include <uart.h>

#include <thread>


int main(int argc, const char **argv) {
	Uart uart;

	unsigned char CmdStop[] = "z";
	unsigned char CmdFront[] = "A";

	uart.sendUart(&CmdFront[0], 1);

	//std::this_thread::yield();
	std::this_thread::sleep_for(std::chrono::seconds(1));
  	//usleep(1000000);  // 1 sec delay

	uart.sendUart(&CmdStop[0], 1);

	//std::this_thread::sleep_for(std::chrono::milliseconds(500));

	return 0;
}
